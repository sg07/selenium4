package com.pagefunctions;

import static org.openqa.selenium.support.locators.RelativeLocator.with;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.interactions.Actions;


import com.locators.HomePageLocators;

public class HomePageFunctions extends HomePageLocators {

	public HomePageFunctions(){
		super();
		System.out.println("I am in HomePageFunctions");
	}
	public static void login(String user, String pwd) throws InterruptedException{
		driver.findElement(with(By.tagName("input")).above(password)).sendKeys(user);
		//Thread.sleep(3000);
		password.sendKeys(pwd);
		
		//Thread.sleep(3000);
		driver.findElement(with(By.tagName("input")).below(password)).click();
		
	}
	
	public static void ActionClick()
	{
		Actions action = new Actions(driver);
		packageListing();
		action.contextClick(searchBox).perform();
	
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		action.click(addPackageButton).perform();
	}
	
	public static void WindowManagement()
	{
		driver.switchTo().newWindow(WindowType.TAB);
		driver.get("https://www.google.com");
	}
}
