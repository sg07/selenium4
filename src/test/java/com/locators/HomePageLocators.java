package com.locators;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static org.openqa.selenium.support.locators.RelativeLocator.*;

import com.utility.base.TestBase;

public class HomePageLocators extends TestBase{

	
	public static WebElement password ;
	
	public static WebElement addPackageButton; 
	
	public static WebElement searchBox;

	public HomePageLocators(){
		super();
		try
		{
		
			password = driver.findElement(By.id("ctl00_PageContentPlaceholder_PasswordInput"));
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}


	public static void packageListing()
	{
		addPackageButton = driver.findElement(By.id("createClicked"));
		searchBox = driver.findElement(By.xpath("//input[@type='text']"));
	}
}