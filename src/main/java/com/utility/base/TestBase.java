package com.utility.base;

import io.github.bonigarcia.wdm.WebDriverManager;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;



public class TestBase {
	
	public static WebDriver driver ;
	public static String url = "https://customerengagement.zaidyn.qa.zsservices.com/ccm-0079ver4/admin/setup-and-resource-files/packages/abcde";
	
	String Node = "http://localhost:4444";
	
	public TestBase(){
		DesiredCapabilities caps = new DesiredCapabilities();
        caps.setBrowserName("firefox");
		WebDriverManager.firefoxdriver().setup();
		//ChromeOptions options = new ChromeOptions();
		//driver = new ChromeDriver(options);
		try {
			driver = new RemoteWebDriver(new URL(Node), caps);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.manage().window().maximize();
		driver.get(url+"abcd");
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		
		
	}

//	public static void navigateToUrl(String url){
//		driver.get(url);
//		
//	}
}
