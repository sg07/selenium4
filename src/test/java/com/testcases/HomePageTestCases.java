package com.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import static org.openqa.selenium.support.locators.RelativeLocator.*;

import org.openqa.selenium.By;
import org.openqa.selenium.support.locators.RelativeLocator;

import com.locators.HomePageLocators;
import com.pagefunctions.HomePageFunctions;

public class HomePageTestCases extends HomePageFunctions {
	
	public HomePageTestCases() {

		super();
	}
	
	@Test(priority = 0)
	public void verifyLogin() throws InterruptedException{
//		navigateToUrl(url);	
		login("OE_Selenium","Selenium@2024");
	}
	
	@Test(priority = 1)
	public void verifyActionClass()
	{
		ActionClick();
	}
	
	@Test(priority = 0)
	public void verifyTabManagement()
	{
		WindowManagement();
	}
}
